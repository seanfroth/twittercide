#!/usr/bin/python

import sys
import re
import datetime
import os
import os.path
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#Attempt to connect to Selenium first, in case it's not configured correctly
browser = webdriver.Chrome()

if len(sys.argv) >= 3:
    username_text = sys.argv[1]
    password_text = sys.argv[2]
else:
    username_text = input('Enter your username:')
    password_text = input('Enter your password:')

#log into Twitter
browser.get("https://twitter.com/login")

browser.maximize_window()

username = browser.find_element_by_class_name("js-username-field")
password = browser.find_element_by_class_name("js-password-field")

username.send_keys(username_text)
wait = WebDriverWait(browser, 1, 100)
password.send_keys(password_text)

wait = WebDriverWait(browser, 3, 100)
browser.find_element_by_css_selector("button.submit.EdgeButton.EdgeButton").click()

wait = WebDriverWait(browser, 3, 100)

#Twitter has rate limits: 100-click limit for every hour, or under other cirumstances 180-click limit per 15 minutes
clicked = 0
clicked_max = 180

############ https://twitter.com/i/likes

while clicked < clicked_max:
    try:
        browser.get('https://twitter.com/i/likes')
        WebDriverWait(browser, 1, 200)
        undoLikeButtons = browser.find_elements_by_css_selector('button.ProfileTweet-actionButtonUndo.ProfileTweet-action--unfavorite.u-linkClean.js-actionButton.js-actionFavorite')#browser.find_element_by_class_name('ProfileTweet-actionButtonUndo')
        if len(undoLikeButtons) == 0:
            break
        undidOne = False
        for undoLikeButton in undoLikeButtons:
            try:
                undoLikeButton.click()
                WebDriverWait(browser, 1, 10)
                clicked = clicked + 1
                undidOne = True
            except:
                continue
        if undidOne == False:
            didOne = False
            likeButtons = browser.find_elements_by_css_selector('button.ProfileTweet-actionButton.js-actionButton.js-actionFavorite')#find_element_by_class_name('ProfileTweet-actionButton')
            for likeButton in likeButtons:
                try:
                    likeButton.click()
                    WebDriverWait(browser, 1, 10)
                    clicked = clicked + 1
                    didOne = True
                except:
                    continue
            if didOne == False:
                break
    except:
        break


########### like.js file
#open file
if os.path.isfile('like.js'):
    wroteFile = False
    with open('like.js') as f:
        for line in f:
            if clicked >= clicked_max:
                if wroteFile == False:
                    w = open('like.js.temp','w')
                    wroteFile = True
                w.write(line)
            else:
                matchObj = re.match( r'\s*"tweetId"\s*:\s*"(\d*)"\s*', line, re.M|re.I)
                if matchObj:
                    #get Tweet
                    tweetID = matchObj.group(1)

                    browser.get('https://twitter.com/anyuser/status/' + tweetID)

                    try:
                        tweet = browser.find_element_by_xpath("//div[@data-tweet-id='" + tweetID + "']")
                        likeButton = tweet.find_element_by_css_selector('button.ProfileTweet-actionButton.js-actionButton.js-actionFavorite')#find_element_by_class_name('ProfileTweet-actionButton')
                        undoLikeButton = tweet.find_element_by_css_selector('button.ProfileTweet-actionButtonUndo.ProfileTweet-action--unfavorite.u-linkClean.js-actionButton.js-actionFavorite')#browser.find_element_by_class_name('ProfileTweet-actionButtonUndo')

                        if likeButton.text.startswith('Like'):
                            likeButton.click()
                            WebDriverWait(tweet, 5, 100)
                            browser.get('https://twitter.com/anyuser/status/' + tweetID)
                            tweet = browser.find_element_by_xpath("//div[@data-tweet-id='" + tweetID + "']")
                            undoLikeButton = tweet.find_element_by_css_selector('button.ProfileTweet-actionButtonUndo.ProfileTweet-action--unfavorite.u-linkClean.js-actionButton.js-actionFavorite')#browser.find_element_by_class_name('ProfileTweet-actionButtonUndo')
                            undoLikeButton.click()
                            WebDriverWait(tweet, 1, 100)
                            clicked = clicked + 2
                        elif undoLikeButton.text.startswith('Liked'):
                            undoLikeButton.click()
                            WebDriverWait(tweet, 1, 100)
                            clicked = clicked + 1
                        else:
                            break
                    except:
                        continue
    if wroteFile == True:
        f.close()
        w.close()
        os.remove('like.js')
        os.rename('like.js.temp', 'like.js')
    else:
        os.remove('like.js')#File is now complete

browser.quit()
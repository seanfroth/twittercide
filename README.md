# Twittercide

Selenium based API for purging all likes/favorites from a profile. The "chromedriver" module (https://sites.google.com/a/chromium.org/chromedriver/) must be installed and in PATH variable.

There is support for loading the "like.js" file as part of the "Your Tweet archive" data export. This will allow for likes that are older than 90 days, as they are not available through the normal API. As of development access to this export is rate limted to one per month, making the usage frustrating.

Due to rate limitation, the maximum amount of un-likes is 180 per 15 minutes. Given a "chron" task this can allow for quickly purging huge volumes of old likes.

Works as-of 2019-01-17.